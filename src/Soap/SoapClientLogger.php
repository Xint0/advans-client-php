<?php

/**
 * Advans Client PHP
 *
 * @copyright Copyright 2022 Rogelio Jacinto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package advans-client-php
 * @license https://gitlab.com/xint0/advans-client-php/-/blob/main/LICENSE Apache License 2.0
 */

declare(strict_types=1);

namespace Xint0\AdvansClientPhp\Soap;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SoapClient;
use Throwable;

/**
 * Logs SoapClient calls.
 *
 * Wrapper for a SoapClient that logs and forwards method calls to the wrapped
 * SoapClient instance.
 */
class SoapClientLogger implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private SoapClient $client;

    /**
     * Creates a new instance of the \Xint0\AdvansClientPhp\Soap\SoapClientLogger class.
     *
     * @param SoapClient $client The SoapClient instance to wrap.
     * @param LoggerInterface|null $logger Optional. The \Psr\Log\LoggerInterface
     *   used to log method calls. If not specified a logging is disabled.
     */
    public function __construct(SoapClient $client, ?LoggerInterface $logger = null)
    {
        $this->client = $client;
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * Forwards call to wrapped SoapClient and logs method call details.
     *
     * @param string $method
     * @param array $params
     *
     * @return false|mixed
     *
     * @throws Throwable
     */
    public function __call(string $method, array $params)
    {
        try {
            $result = call_user_func_array([$this->client, $method], $params);
        } catch (Throwable $exception) {
            $this->logger->error(__METHOD__ . " - '{$method}' call failed.", [
                'method' => $method,
                'exception' => $exception,
                'request' => $this->client->__getLastRequest(),
                'requestHeaders' => $this->client->__getLastRequestHeaders(),
            ]);

            throw $exception;
        }

        $this->logger->debug(
            __METHOD__ . " - '{$method}' call succeeded.",
            [
                'method' => $method,
                'request' => $this->client->__getLastRequest(),
                'requestHeaders' => $this->client->__getLastRequestHeaders(),
                'response' => $this->client->__getLastResponse(),
                'responseHeaders' => $this->client->__getLastResponseHeaders(),
                'result' => $result,
            ]
        );

        return $result;
    }
}