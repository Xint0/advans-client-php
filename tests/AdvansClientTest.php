<?php

/**
 * Advans Client PHP
 *
 * @copyright Copyright 2022 Rogelio Jacinto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package advans-client-php
 * @license https://gitlab.com/xint0/advans-client-php/-/blob/main/LICENSE Apache License 2.0
 */

declare(strict_types=1);

namespace Xint0\AdvansClientPhp\Tests;

use Xint0\AdvansClientPhp\AdvansClient;
use PHPUnit\Framework\TestCase;

class AdvansClientTest extends TestCase
{
    public function test_creates_instance_with_expected_params(): void
    {
        $params = [
            'base_url' => 'https://dev.advans.mx',
            'key' => '8d3ba1ff600a9ac056d923429ef97886',
        ];
        $sut = new AdvansClient($params);
        $this->assertInstanceOf(AdvansClient::class, $sut);
        $this->assertEquals($params, $sut->params());
    }
}
