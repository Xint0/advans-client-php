<?php

/**
 * Advans Client PHP
 *
 * @copyright Copyright 2022 Rogelio Jacinto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package advans-client-php
 * @license https://gitlab.com/xint0/advans-client-php/-/blob/main/LICENSE Apache License 2.0
 */

declare(strict_types=1);

namespace Xint0\AdvansClientPhp\Tests\Soap;

use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SoapClient;
use SoapFault;
use Throwable;
use Xint0\AdvansClientPhp\Soap\SoapClientLogger;
use PHPUnit\Framework\TestCase;

class SoapClientLoggerTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function test_can_create_instance_without_logger(): void
    {
        $wsdl = 'https://dev.advans.mx/ws/awscfdi.php?wsdl';
        $options = [
            'trace' => true,
        ];

        $soapClient = Mockery::mock(SoapClient::class, [$wsdl, $options]);
        $sut = new SoapClientLogger($soapClient);
        $this->assertNotNull($sut);
    }

    public function test_logs_last_request_when_method_calls_throws_exception(): void
    {
        $wsdl = 'https://dev.advans.mx/ws/awscfdi.php?wsdl';
        $options = [
            'trace' => true,
        ];
        $request = '<request></request>';
        $requestHeaders = 'Request-Headers: value';
        $soapFault = new SoapFault('code', 'message', 'actor');
        $mockSoapClient = Mockery::mock(SoapClient::class, [$wsdl, $options]);
        $mockSoapClient->shouldReceive('timbrar')->andThrow($soapFault);
        $mockSoapClient->shouldReceive()->__getLastRequest()->andReturn($request);
        $mockSoapClient->shouldReceive()->__getLastRequestHeaders()->andReturn($requestHeaders);
        $mockSoapClient->makePartial();
        $mockLogger = Mockery::spy(LoggerInterface::class);
        $sut = new SoapClientLogger($mockSoapClient);
        $sut->setLogger($mockLogger);
        try {
            $sut->timbrar();
        } catch (Throwable $exception) {
            $this->assertSame($soapFault, $exception);
        }

        $mockLogger->shouldHaveReceived()
            ->error(
                "Xint0\AdvansClientPhp\Soap\SoapClientLogger::__call - 'timbrar' call failed.",
                [
                    'method' => 'timbrar',
                    'exception' => $soapFault,
                    'request' => $request,
                    'requestHeaders' => $requestHeaders,
                ],
            );
    }

    public function test_logs_last_request_and_response_when_method_call_is_successful(): void
    {
        $wsdl = 'https://dev.advans.mx/ws/awscfdi.php?wsdl';
        $options = [
            'trace' => true,
        ];
        $request = '<request></request>';
        $requestHeaders = 'Request-Headers: value';
        $response = '<response></response>';
        $responseHeaders = 'Response-Headers: value';
        $result = (object) [
            'key' => 'value',
        ];
        $mockSoapClient = Mockery::mock(SoapClient::class, [$wsdl, $options]);
        $mockSoapClient->shouldReceive('timbrar')
            ->andReturn($result);
        $mockSoapClient->shouldReceive()->__getLastRequest()->andReturn($request);
        $mockSoapClient->shouldReceive()->__getLastRequestHeaders()->andReturn($requestHeaders);
        $mockSoapClient->shouldReceive()->__getLastResponse()->andReturn($response);
        $mockSoapClient->shouldReceive()->__getLastResponseHeaders()->andReturn($responseHeaders);
        $mockSoapClient->makePartial();
        $mockLogger = Mockery::spy(LoggerInterface::class);
        $sut = new SoapClientLogger($mockSoapClient);
        $sut->setLogger($mockLogger);
        $actualResult = $sut->timbrar();

        $this->assertEquals($result, $actualResult);
        $mockLogger->shouldHaveReceived()->debug(
            "Xint0\AdvansClientPhp\Soap\SoapClientLogger::__call - 'timbrar' call succeeded.",
            [
                'method' => 'timbrar',
                'request' => $request,
                'requestHeaders' => $requestHeaders,
                'response' => $response,
                'responseHeaders' => $responseHeaders,
                'result' => $result,
            ]
        );
    }
}
